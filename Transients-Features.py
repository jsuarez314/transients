#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from tqdm import tqdm
import sys
import os
import subprocess
import sep


# In[2]:


def SN(img): #Signal/Noise
    ave_int=np.average(img)
    des_int=np.std(img)
    sn=ave_int/des_int
    return sn

def Flux(img): #Flux
    img = img.astype('double')
    bkg= sep.Background(img, bw=32, bh=32, fw=3, fh=3)
    bkg_image_original = bkg.back()
    bkg.subfrom(img)
    flux, fluxerr, flag = sep.sum_circle(img, 32, 32, 4.0, err = bkg.globalrms, gain = 1.0)
    flux = np.expand_dims(flux, axis=0)
    return flux

def CAT(labels,idd): # Return category
    return np.load(labels,allow_pickle=True).tolist()[idd]


# In[3]:


#/home/toshiba/Development/GPU/Astronomy/Data/AGN/CSS071204:100029+071116/01_08APR11_N07053_0004_cutout_0218_ROI_all.npy


# In[4]:


# fig=plt.figure(figsize=(4,4))
# for i in range(1,5):
# #     file="01_06SEP18_S01011_000"+str(i)+"_cutout_1623_ROI_all.npy"
#     file="/home/toshiba/Development/Transients/Astronomy/Data/AGN/CSS071204:100029+071116/01_08APR11_N07053_0004_cutout_0218_ROI_all.npy"
#     A = np.load(file)
#     plt.subplot(2,2,i)
#     plt.imshow(A)
#     plt.plot()


# In[5]:


# fig=plt.figure(figsize=(4,4))
# for i in range(1,5):
#     A = np.load("01_06OCT22_S01011_000"+str(i)+"_cutout_1623_ROI_all.npy")
#     plt.subplot(2,2,i)
#     plt.imshow(A)
#     plt.plot()


# In[6]:


# fig=plt.figure(figsize=(4,4))
# for i in range(1,5):
#     A = np.load("02_06JUN19_S09116_000"+str(i)+"_cutout_1727_ROI_all.npy")
#     plt.subplot(2,2,i)
#     plt.imshow(A)
#     plt.plot()


# ### Objects

# In[7]:


#path_astro="/home/toshiba/Development/Transients/Astronomy/"
path_astro="/home/jfsuarez/Transients/"
labels=path_astro+"configs/labels_base_noAug_TnT_FinalTrain_CompleteEval.npy"


# ###  Validation

# In[8]:


classes=['BZ', 'AGN', 'CV', 'OTHER', 'SN','NON']
train_validation_file=path_astro+"configs/partitions_base_noAug_TnT_FinalTrain_CompleteEval.npy"
tvf=np.load(train_validation_file,allow_pickle=True)
validation_labels=np.array(list(tvf.tolist()['validation'])) # Labels of validation object
train_labels=np.array(list(tvf.tolist()['train'])).shape     # Labels of train object
probabilities_file=path_astro+"Models/multiclass/validation_19_outputs.npy"
probabilities=np.load(probabilities_file)                    # Probabilities of allow to class 
cat_file=path_astro+"Models/multiclass/validation_19_targets.npy"
cat=np.load(cat_file)                         # Category for object


# In[9]:


# _=plt.hist(predictions,bins=5)
# _=plt.grid()


# In[10]:


#path_DBFull="/home/toshiba/Development/Transients/Astronomy/configs"
path_DBFull="/home/jfsuarez/Transients/configs/"
DB_Full=pd.read_pickle(path_DBFull+"/NEWcompleteDF_BZSeason.pkl")
# DB_Full.dtypes


# In[11]:


#print(DB_Full["CRTS ID"])


# In[12]:


# f1_score(true_cat_cat, predictions, average='weighted')
#0.7066844523491924    


# ### DataFrame

# In[13]:


#path="/home/toshiba/Development/Transients/Astronomy/Data/"
path="/mnt/data/tarFolders/"

#0:bz, 1:agn, 2:cv, 3:other, 4:sn, 5:no_transiente
cols=['CRTS_ID','Path','Dates','Classification','N_Seq','N_Total','Flux_Med','SN_Av','Amp','Prediction','Probabilities']
df = pd.DataFrame(columns=cols)
pbar = tqdm(total=len(validation_labels))
black_list=[]
kk=0
for i in validation_labels:
    clas=str(classes[CAT(labels,i)])
    localpath=path+clas+"/"+i+"/"
    NTot = subprocess.run(["ls "+str(localpath+"*.npy")],shell=True,stdout=subprocess.PIPE).stdout.decode("utf-8").split("\n")[:-1]              #Total Images    
    NTot = len(NTot)
    namefiles_seq=localpath+"*_0001*.npy"    
    sequences = subprocess.run(["ls "+str(namefiles_seq)],shell=True,stdout=subprocess.PIPE).stdout.decode("utf-8").split("\n")[:-1]          #Number of Sequences, all with _0001 label
    dates = [ u[len(localpath)+3:len(localpath)+10] for u in sequences ]   
    dates = np.unique(dates)
    Nseq = len(dates)
    if Nseq==0:
        black_list.append(i)
        continue
    flux=[]
    sn=[]
    amp=[]
    MAX=np.max(np.load(sequences[0]))
    MIN=np.min(np.load(sequences[0]))
    for j in sequences:
        A=np.load(j)
        if np.std(A)==0:
            continue
        sn.append(SN(A))
        flux.append(Flux(A))
        maxx=np.max(A)
        minn=np.min(A)
        if maxx>MAX:
            MAX=maxx
        if minn<MIN:
            MIN=minn         
    sn_av=np.average(sn)
    flux_med=np.median(flux)
    amp=MAX-MIN

    data=pd.DataFrame([[i,localpath,dates,cat[kk],Nseq,NTot,flux_med,sn_av,amp,np.argmax(probabilities[kk]),probabilities[kk]]],columns=cols)
    df=df.append(data,ignore_index=True)
    kk=kk+1
    pbar.update(1)
pbar.close()
df.to_pickle('Analysis_Transients.pkl')
# df
